<?php
/**
 * Wordpress INTR THEME framework
 *
 * Version 1.0
 * Date: 06.11.2017
 *
 * @package WordPress
 * @subpackage Timber for INTR THEME 
 *
 */

$GLOBALS['timberContext'] = Timber::get_context();
ob_start();

?>