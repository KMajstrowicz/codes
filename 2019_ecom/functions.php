<?php
/**
 * Wordpress INTR THEME framework
 *
 * Version 1.0
 * Date: 06.11.2017
 *
 * @package WordPress
 * @subpackage Timber for INTR THEME 
 *
 */

Timber::$dirname = array('templates/views', 'templates/views');

class StarterSite extends TimberSite {
    function __construct() {
        add_theme_support( 'post-formats' );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'menus' );
        add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
        add_filter( 'timber_context', array( $this, 'add_to_context' ) );
        add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
        add_action( 'init', array( $this, 'register_post_types' ) );
        add_action( 'init', array( $this, 'register_taxonomies' ) );
        parent::__construct();
    }
    function register_post_types() {
        
        $umbrella = array(
            'labels' => array (
                'name' => 'Parasolki',
                'sungular_name' => 'Parasolka',
                'all_items' => 'Wszystkie',
                'add_new' => 'Dodaj',
                'add_new_item' => 'Dodaj',
                'edit_item' => 'Edytuj',
                'new_item' => 'Nowy',
                'view_item' => 'Zobacz',
                'search_items' => 'Szukaj',
                'not_found' => 'Nie znaleziono',
                'not_found_in_trash' => 'Nie znaleziono w koszu',
                'parent_item_colon' => ''
            ),
            'public' => true,
            'public_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'umbrella'),
            'capatility_type' => 'post',
            'hierarchical' => true,
            'menu_position' => 5,
            'show_in_rest' => true,
                'rest_base' => 'umbrella',
                'rest_controller_class' => 'WP_REST_Posts_Controller',
            'supports' => array(
                'title', 'editor', 'author'
            ),
        );
        register_post_type('umbrella', $umbrella);

        $slider = array(
            'labels' => array (
                'name' => 'Slidery',
                'sungular_name' => 'Slider',
                'all_items' => 'Wszystkie',
                'add_new' => 'Dodaj',
                'add_new_item' => 'Dodaj',
                'edit_item' => 'Edytuj',
                'new_item' => 'Nowy',
                'view_item' => 'Zobacz',
                'search_items' => 'Szukaj',
                'not_found' => 'Nie znaleziono',
                'not_found_in_trash' => 'Nie znaleziono w koszu',
                'parent_item_colon' => ''
            ),
            'public' => true,
            'public_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'slider'),
            'capatility_type' => 'post',
            'hierarchical' => true,
            'menu_position' => 5,
            'show_in_rest' => true,
            'rest_base' => 'slider',
            'rest_controller_class' => 'WP_REST_Posts_Controller',
            'supports' => array(
                'title', 'editor', 'author'
            ),
        );
        register_post_type('slider', $slider);

    }

    function register_taxonomies() {
        //this is where you can register custom taxonomies
    }
    function add_to_context( $context ) {
        $context['foo'] = WP_CONTENT_URL.'/themes/'.get_template().'/';
        $context['stuff'] = 'I am a value set in your functions.php file';
        $context['notes'] = 'These values are available everytime you call Timber::get_context();';
        $context['menu_main'] = new TimberMenu('menu_main');
        $context['menu_user'] = new TimberMenu('menu_user');
        $context['menu_footer_categories'] = new TimberMenu('menu_footer_categories');
        $context['menu_footer_brands'] = new TimberMenu('menu_footer_brands');
        $context['menu_footer_main'] = new TimberMenu('menu_footer_main');
        $context['site'] = $this;

        $context['umbrella'] = Timber::get_posts(array(
                'post_type' => 'umbrella',
                'posts_per_page' => -1
            )
        );
        $context['sliders'] = Timber::get_posts(array(
                'post_type' => 'slider',
                'posts_per_page' => -1
            )
        );
        $context['options'] = get_fields('options');

        return $context;
    }
    function add_to_twig( $twig ) {
        /* this is where you can add your own functions to twig */
        $twig->addExtension( new Twig_Extension_StringLoader() );
        return $twig;
    }
}
new StarterSite();

if( function_exists('acf_add_options_page') ) {
 	
	acf_add_options_page(array(
		'page_title' 	=> 'Ustawienia aplikacji',
		'menu_title' 	=> 'Ustawienia aplikacji',
		'redirect' 		=> false
	));
	
}

register_nav_menus(array(
    'menu_main' => 'Menu main',
    'menu_user' => 'Menu user',
    'menu_footer_categories' => 'Footer menu categories',
    'menu_footer_main' => 'Footer menu main',
    'menu_footer_brands' => 'Footer menu brands',
));