<?php
/**
 * Wordpress INTR THEME framework
 *
 * Version 1.0
 * Date: 06.11.2017
 *
 * @package WordPress
 * @subpackage Timber for INTR THEME
 *
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
Timber::render( array( 'page-' . $post->post_name . '.twig', 'page.twig' ), $context ); 

?>