<?php
/**
 *
 * Template Name: Widok 003 (Triwise)
 *
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>


<section class="subpage-wrapper">

	<div class="container">

		<div class="subpage-wrapper_header">
			<h1><?php the_title(); ?></h1>
		</div>

        <?php if (function_exists('yoast_breadcrumb')) { yoast_breadcrumb('<p class="breadcrumbs">','</p>'); } ?>

		<div class="subpage-wrapper_content">

            <?php the_content(); ?>

		</div>

	</div>

	<div class="subpage-wrapper_image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>

</section>


<?php
	$selectValue = get_field('timeline_select');

	if ($selectValue === "info_boxes"){
		require(THEME_DIR.'/_modules/_info-boxes.php');
    } elseif ($selectValue === "timeline") {
        require(THEME_DIR.'/_modules/_timeline.php');
	} elseif ($selectValue === "find_us") {
		require(THEME_DIR.'/_modules/_find-us.php');
    }  elseif ($selectValue === "numbers") {
		require(THEME_DIR.'/_modules/_info-boxes-numbers.php');
    } elseif ($selectValue === "all") {
		require(THEME_DIR.'/_modules/_info-boxes.php');
		require(THEME_DIR.'/_modules/_timeline.php');
		require(THEME_DIR.'/_modules/_find-us.php');
		require(THEME_DIR.'/_modules/_about-us.php');
    }
?>


<?php require(THEME_DIR.'/_modules/_CTA-join-us.php'); ?>


<?php require(THEME_DIR.'/_modules/_partners.php'); ?>


<?php require(THEME_DIR.'/_modules/_footer-nav.php'); ?>


<?php require(THEME_DIR.'/_modules/_footer-bar.php'); ?>


<?php get_footer(); ?>
