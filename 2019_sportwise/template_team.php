<?php
/**
 *
 * Template Name: Widok 002 (Team)
 *
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>


<section class="subpage-wrapper">

	<div class="container">

		<div class="subpage-wrapper_header">
			<h1><?php the_title(); ?></h1>
		</div>

        <?php if (function_exists('yoast_breadcrumb')) { yoast_breadcrumb('<p class="breadcrumbs">','</p>'); } ?>

		<div class="subpage-wrapper_content">

            <?php the_content(); ?>

		</div>

	</div>

	<div class="subpage-wrapper_image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>

</section>


<?php require(THEME_DIR.'/_modules/_find-us.php'); ?>


<?php require(THEME_DIR.'/_modules/_about-us.php'); ?>


<?php require(THEME_DIR.'/_modules/_CTA-join-us.php'); ?>


<?php require(THEME_DIR.'/_modules/_partners.php'); ?>


<?php require(THEME_DIR.'/_modules/_footer-nav.php'); ?>


<?php require(THEME_DIR.'/_modules/_footer-bar.php'); ?>


<?php get_footer(); ?>
