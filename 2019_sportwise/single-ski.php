<?php get_header(); ?>
<?php the_post(); ?>

<article class="single-article">

	<div class="container">

		<h1 class="heading"><?php the_title(); ?></h1>

		<div class="single-article_header">

			<div class="single-article_category"><?php the_category(); ?></div>

			<span class="single-article_date"><?php echo get_the_date(); ?></span>

			<?php echo do_shortcode('[addtoany]'); ?>

		</div>

        <?php
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
			}
        ?>

		<div class="single-article_content">

			<div class="thumbnail" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>

			<?php the_content(); ?>

		</div>

		<div id="js-articles">
			<div class="articles">
				<article v-for="(article, index) in posts.slice(0, 3)" class="article">
					<figure class="article-thumbnail" :style="'background-image: url(' + getThumbnail(article) + ');'">
						<div class="article-categories">
							<div class="article-category">Kategoria</div>
						</div>
					</figure>
					<div class="article-title"> {{ article.title.rendered }} </div>
					<div class="article-excerpt" v-html="article.excerpt.rendered"></div>
					<div class="article-data"> {{ getData(article) }} </div>
				</article>
			</div>
		</div>

	</div>


</article>


<?php require(THEME_DIR.'/_modules/_CTA-join-us.php'); ?>


<?php require(THEME_DIR.'/_modules/_partners.php'); ?>


<?php require(THEME_DIR.'/_modules/_footer-nav.php'); ?>


<?php require(THEME_DIR.'/_modules/_footer-bar.php'); ?>


<?php get_footer(); ?>
