<?php 
    get_header();
    $pageTitle = is_home() ? 'Aktualności' : get_the_title(); 
?>

    <div class="container">
        <div class="artiles-container" id="js-articles" v-cloak>
            <div class="articles">
                <div class="headline">
                    <div class="section_heading">
                        <h2><?php echo $pageTitle; ?><span>({{ posts.length }})</span></h2>
                    </div>
                </div>
                <div class="articles-topbar">
                    <div class="articles-categories">
                        <div class="article-category">
                            <span :class="currentPostCategory === null ? 'article-category-link is-active' : 'article-category-link'" @click="getPostsFromCategory()">Wszystkie</span>
                        </div>
                        <div class="article-category" v-for="category in categories">
							<span :class="currentPostCategory === category.id ? 'article-category-link is-active' : 'article-category-link'" @click="getPostsFromCategory(category.id)">{{ category.name }}</span>
                        </div>
                    </div>
                    <div class="articles-sort">
                        <select class="articles-sort-select" @change="sortPosts" name="sort_articles">
                            <option selected disabled>Sortuj</option>
                            <option value="asc">Od najnowszych</option>
                            <option value="desc">Od najstarszych</option>
                        </select>
                    </div>
                </div>
                <div class="loader-container" v-if="loading">
                    <div class="loader-image">
                        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/loader.svg' ?>" alt="ładuję...">
                    </div>
                </div>
                <template v-if="posts.length">
                    <article v-for="(article, index) in posts" :class="getArticleTypeClasses(index, article, 'article')">
                        <a :href="article.link">
                            <figure class="article-thumbnail" :style="'background-image: url(' + getThumbnail(article) + ');'">
                                <div class="article-categories">
                                    <div class="article-category" v-if="article.acf.featured">
                                        <span class="article-category-link is-featured">Wyróżniony</span>
                                    </div>
                                    <div class="article-category" v-for="category in article.postCategories">
                                        <object>
                                            <a :href="getPostCategorLink(category.term_id)" class="article-category-link">{{ category.name }}</a>
                                        </object>
                                    </div>
                                </div>
                            </figure>
                        </a>
                        <a :href="article.link">
                            <div class="article-title"> {{ article.title.rendered }} </div>
                            <div class="article-excerpt" v-html="article.excerpt.rendered"></div>
                            <div class="article-data"> {{ getData(article) }} </div>
                        </a>
                    </article>
                </template>
                <div v-else class="articles-empty"></div>
            </div>
        </div>
    </div>

    <?php require(THEME_DIR.'/_modules/_partners.php'); ?>


    <?php require(THEME_DIR.'/_modules/_footer-nav.php'); ?>


    <?php require(THEME_DIR.'/_modules/_footer-bar.php'); ?>



<?php get_footer(); ?>
