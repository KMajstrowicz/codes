<?php
/**
 *
 * Template Name: Widok 004 (Kontakt)
 *
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>


<section class="subpage-wrapper subpage-contact">

	<div class="container">

		<div class="subpage-wrapper_header">
			<h1><?php the_title(); ?></h1>
		</div>


        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
        }
        ?>

		<div class="subpage-wrapper_content">

            <?php the_content(); ?>

		</div>

	</div>

	<div class="subpage-wrapper_image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>

</section>


<section class="contact-form">

	<div class="container">

        <?php echo do_shortcode('[contact-form-7 id="275" title="Formularz kontaktowy"]'); ?>

	</div>

</section>


<?php require(THEME_DIR.'/_modules/_partners.php'); ?>


<?php require(THEME_DIR.'/_modules/_footer-nav.php'); ?>


<?php require(THEME_DIR.'/_modules/_footer-bar.php'); ?>


<?php get_footer(); ?>
