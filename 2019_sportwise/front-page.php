<?php get_header(); ?>



	<?php require(THEME_DIR.'/_modules/_carousel.php'); ?>


	<?php require(THEME_DIR.'/_modules/_mosaic.php'); ?>


	<?php require(THEME_DIR.'/_modules/_news.php'); ?>


	<?php require(THEME_DIR.'/_modules/_our-mission.php'); ?>


	<?php require(THEME_DIR.'/_modules/_about-us.php'); ?>


	<?php require(THEME_DIR.'/_modules/_find-us.php'); ?>


	<?php require(THEME_DIR.'/_modules/_schedule.php'); ?>


	<?php require(THEME_DIR.'/_modules/_testimonials.php'); ?>


	<?php require(THEME_DIR.'/_modules/_CTA-join-us.php'); ?>


	<?php require(THEME_DIR.'/_modules/_partners.php'); ?>


	<?php require(THEME_DIR.'/_modules/_footer-nav.php'); ?>


	<?php require(THEME_DIR.'/_modules/_footer-bar.php'); ?>


	<!--  PopUp	-->
	<?php require(THEME_DIR.'/_modules/_fu-popup.php'); ?>


<?php get_footer(); ?>
