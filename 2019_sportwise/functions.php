<?php

if(!defined('THEME_DIR')) {
    define('THEME_DIR', get_theme_root(). '/' .get_template() . '/');
}

if(!defined('THEME_URL')) {
    define('THEME_URL', WP_CONTENT_URL. '/themes/' .get_template() . '/');
}



// ACF OPTIONS PAGE

if( function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' 	=> 'Ustawienia',
        'post_id' => 'options',
        'menu_title'	=> 'Ustawienia',
        'menu_slug' 	=> 'module-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));
}



// NAV REGISTER

register_nav_menus(array(
    'primary' => 'Main navigation'
));
register_nav_menus(array(
    'secondary' => 'Footer col-0'
));
register_nav_menus(array(
    'tertiary' => 'Footer col-1'
));
register_nav_menus(array(
    'quaternary' => 'Footer col-2'
));

add_theme_support( 'post-thumbnails' );

add_image_size( 'thumbnailArticle', 768, 480 );


// CUSTOM POST TYPE

 function ski() {
   register_post_type( 'Wyjazdy',
     array(
       'labels' => array(
         'name' => __( 'Wyjazdy' ),
         'singular_name' => __( 'ski' )
       ),
       'public' => true,
       'has_archive' => true,
 	     'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
     )
   );
 }
 add_action( 'init', 'ski' );


function prepare_rest_api($data, $post, $request) {
    $_data = $data->data;

    $postCategories = get_the_category($post->ID);

    $_data['postCategories'] = $postCategories;

    $data->data = $_data;

    return $data;
}
add_filter('rest_prepare_post', 'prepare_rest_api', 10, 3);


add_action('init','custom_post_types');
  function custom_post_types(){

    /**
    *  Register harmonograms
    *
    */
    $harmonograms = array(
      'labels' => array (
          'name' => 'Harmonogramy',
          'sungular_name' => 'Harmonogram',
          'all_items' => 'Wszystkie',
          'add_new' => 'Dodaj',
          'add_new_item' => 'Dodaj',
          'edit_item' => 'Edytuj',
          'new_item' => 'Nowy',
          'view_item' => 'Zobacz',
          'search_items' => 'Szukaj',
          'not_found' => 'Nie znaleziono',
          'not_found_in_trash' => 'Nie znaleziono w koszu',
          'parent_item_colon' => ''
      ),
      'public' => true,
      'public_queryable' => true,
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'harmonograms'),
      'capatility_type' => 'post',
      'hierarchical' => true,
      'menu_position' => 5,
      'show_in_rest'       => true,
        'rest_base'          => 'harmonograms',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
      'supports' => array(
          'title', 'editor', 'author'
      ),
    );
    register_post_type('harmonograms', $harmonograms);


  }


    add_action('init','init_taxonomies');

    function init_taxonomies() {
    register_taxonomy(
      'place',
      array('harmonograms'),
      array(
        'hierarchical' => true,
        'labels' => array(
          'name' => 'Lokalizacja',
          'add_new_item' => 'Dodaj'
        ),
        'show_ui' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'place'),
        'show_in_rest' => true,
        'query_var' => true,
        'rest_base'  => 'place'
        )
    );
    register_taxonomy(
      'sport',
      array('sport'),
      array(
        'hierarchical' => true,
        'labels' => array(
          'name' => 'Dyscyplina',
          'add_new_item' => 'Dodaj'
        ),
        'show_ui' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'place'),
        'show_in_rest' => true,
        'query_var' => true,
        'rest_base'  => 'place'
        )
    );
  }