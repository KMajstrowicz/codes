


// *********** //
// MOBILE MENU //
// *********** //

$('#js-menu-toggler').click(function () {
    $('.nav_menu, body').toggleClass('isVisible');
})

var windowWidth = $(window).width();

if ( windowWidth > 1023 )
    $('.nav_menu, body').removeClass('isVisible');


// *********** //
// Header slider //
// *********** //

$(document).ready(function(){
    $('.carousel').slick({
        infinite: true,
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 4000,
        pauseOnHover: false
    });
});

// *********** //
// Testimonials slider //
// *********** //

$(document).ready(function(){
    $('.testimonials-wrapper').slick({
        infinite: true,
        dots: true,
        arrows: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        speed: 500,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });
});

// *********** //
// Partners slider //
// *********** //

$(document).ready(function(){
    $('.partners').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        speed: 500,
        responsive: [
            {
                breakpoint: 1400,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });
});

// *********** //
// Find us popup //
// *********** //

$("#js-fuPopup").click(function () {
    $('.fu-popup').addClass('showed');
    $('body').addClass('of_hidden');
});
$("#js-fuPopupClose").click(function () {
    $('.fu-popup').removeClass('showed');
    $('body').removeClass('of_hidden');
});



function findUsPopUpShowDescrForSelectedPlace() {
    let descrWrapper = $('#descr_wrapper');
    let listOfPlaces = $('#js-fuPopupSelect');

    descrWrapper.children().slideUp();

    listOfPlaces.change(function() {
        let selectedValue = $(this).find("option:selected").val();

        descrWrapper.children().slideUp();
        descrWrapper.find("." + selectedValue).slideDown();
    });

}

findUsPopUpShowDescrForSelectedPlace();