if(document.querySelector('#js-harmonograms')) {
    const harmonograms = new Vue({
        el: '#js-harmonograms',
        data: function() {
            return {
                loading: true,
                places: [],
                harmonograms: []
            }
        },
        methods: {
            getPlaces() {
                axios({
                    method: 'GET',
                    url: '/wp-json/wp/v2/place'
                }).then((response) => {
                    this.places = response.data
                    this.loading = false
                })
            },


            getHarmonograms(event) {
                this.resetHarmonograms()
                
                axios({
                    method: 'GET',
                    url: `/wp-json/wp/v2/harmonograms?place=${event.target.value}`
                }).then((response) => {
                    this.harmonograms = response.data
                    this.loading = false
                })
            },

            /**
            * Turn on loader and clear products array to make loader looking pretty nice
            */
            resetHarmonograms() {
                this.harmonograms = []
                this.loading = true
            }
        },
        mounted: function() {  
            this.getPlaces()
        }
    });
}