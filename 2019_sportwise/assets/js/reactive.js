if (document.querySelector('#js-articles')) {
    const articles = new Vue({
        el: '#js-articles',
        data: function() {
            return {
                loading: true,
                posts: [],
                getPostsQuery: '/wp-json/wp/v2/posts?_embed',
                categories: [],
                currentPostCategory: null
            }
        },
        methods: {
            /**
            * Get posts/articles from blog
            * @param {Object} settings query settings for fetch items 
            */
            getPosts(settings) {
                axios({
                    method: 'GET',
                    url: this.getPostsQuery
                }).then((response) => {
                    this.posts = response.data
                    this.loading = false
                })
            },

            /**
            * Get all categories for posts
            */
            getCategories() {
                axios({
                    method: 'GET',
                    url: '/wp-json/wp/v2/categories?hide_empty=true'
                }).then((response) => {
                    this.categories = response.data
                })
            },

            /**
            * Get post category link by category ID
            * @param {Number} postCategoryId get from post object
            * @return {String}
            */
            getPostCategorLink(postCategoryId) {
                let link = ''
                
                this.categories.map((category) => {
                    
                    // Compare ID from data and from post
                    if (category.id === postCategoryId) {
                        link = category.link 
                    }
                })
                
                return link
            },

            /**
            * Get thumbnail url from API
            * @param {Object} item aricle/post from loop
            * @return {String}
            */
            getThumbnail(item) {
                return item._embedded['wp:featuredmedia'] ? item._embedded['wp:featuredmedia']['0'].media_details.sizes.thumbnailArticle.source_url : '';
            },

            /**
            * Slice date format into more readability by humans
            * @param {Object} item aricle/post from loop
            * @return {String}
            */
            getData(item) {
                return item.date.slice(0,10);
            },

            /**
            * Returns classes for article
            * @param {Number} index came from loop
            * @param {Object} article specified indexed post
            * @param {String} defaultClass default class for single article without attributes
            * @return {String}
            */
            getArticleTypeClasses(index, article, defaultClass) {
                let classes = defaultClass

                // Make element wide than others (2:1)
                if (index === 4) {
                    classes += ' is-wide'
                }

                // Featured article class
                if (article.acf.featured) {
                    classes += ' is-featured'
                }

                if(this.loading === true) {
                    classes += ' is-loading'
                }

                return classes
            },

            /**
            * Modify query with category by id
            * @param {Number} categoryId category id from topbar filters
            */
            getPostsFromCategory(categoryId = 0) {
                this.resetPosts()

                if (!categoryId) {

                    // Remove '&categories=X' from query to see all posts without filtering by category
                    this.getPostsQuery = this.getPostsQuery.replace(/&categories=[0-9]*/, '')
                    this.currentPostCategory = null
                    
                    // run reload posts
                    this.getPosts()

                    return
                }
                
                // set global current category
                this.currentPostCategory = categoryId
                
                // remove previous category ID
                let query = this.getPostsQuery.replace(/&categories=[0-9]*/, '')
                
                // add new category for query and update global query variable
                this.getPostsQuery = query += `&categories=${categoryId}` 

                // run reload posts
                this.getPosts()
            },
            /**
            * Modify query with order by date
            * @param {Number} categoryId category id from topbar filters
            */
            sortPosts(event) {
                this.resetPosts()

                // clear before selected sort value
                let query = this.getPostsQuery.replace(/&filter\[orderby\]=date&order=[a-z]{3,4}/, '')

                // set new sort definition
                this.getPostsQuery = query += `&filter[orderby]=date&order=${event.target.value}`

                // run reload posts
                this.getPosts()
            },
            
            /**
            * Turn on loader and clear products array to make loader looking pretty nice
            */
            resetPosts() {
                this.loading = true
            }
        },
        mounted: function() {   
            this.getCategories()
            this.getPosts()
        }
    });
}