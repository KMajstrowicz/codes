<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset') ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?php wp_title(); ?>
    </title>
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Montserrat:400,500,600|Oswald:400,500" rel="stylesheet">
	<link href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri() . '/assets/css/style.css'?>" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>




<header>

    <?php require(THEME_DIR.'/_modules/_header-nav.php'); ?>

</header>
