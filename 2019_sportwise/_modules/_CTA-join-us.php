<section class="join-us">

	<div class="container">

		<h2><?php the_field('joinus_heading', 'options'); ?></h2>
		<p><?php the_field('joinus_paragraph', 'options'); ?></p>

		<div class="buttons_wrapper">

            <?php if ( have_rows('joinus_buttons', 'options') ) :
				$i = 0 ?>
            	<?php while( have_rows('joinus_buttons', 'options') ) : the_row(); ?>

					<a href="<?php the_sub_field('href'); ?>" class="<?php echo ($i == 0)?'btn btn_tertiary':'btn btn_secondary';?>"><?php the_sub_field('text'); ?></a>

				<?php $i++;
                endwhile; ?>
            <?php endif; ?>

		</div>

	</div>

</section>