<section class="our-mission">

	<div class="container">

		<h2>
			<?php the_field('our_mission_heading','options'); ?>
		</h2>

		<div class="typo">

            <?php the_field('our_mission_content','options'); ?>

		</div>

		<div class="button_wrapper">
			<a href="<?php the_field('our_mission_href','options') ?>" class="btn btn_primary">Dołącz do nas</a>
		</div>

	</div>

	<div class="section_image"></div>

</section>