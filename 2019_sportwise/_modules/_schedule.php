<section class="schedule">

	<div class="section_heading">
		<h2>Harmonogramy zajęć</h2>
	</div>

	<div class="schedule_boxes">

        <?php if ( have_rows('schedule', 'options') ) : while( have_rows('schedule', 'options') ) : the_row(); ?>

				<div class="item">

					<a href="<?php the_sub_field('url', 'options') ?>">
						<img src="<?php the_sub_field('icon', 'options') ?>" alt="icon">
						<h6><?php the_sub_field('text', 'options') ?></h6>
					</a>

				</div>

        <?php endwhile; endif; ?>

	</div>

</section>