<section class="carousel">

    <?php if ( have_rows('slider', 'options') ) : ?>
        <?php while( have_rows('slider', 'options') ) : the_row(); ?>

            <div class="carousel_item" style="background-image: url('<?php the_sub_field('image', 'options') ?>')">

                <div class="container">
                    <h2>
                        <?php the_sub_field('heading', 'options') ?>
                    </h2>
                    <?php the_sub_field('paragraph', 'options') ?>
                    <a href="<?php the_sub_field('link', 'options') ?>" class="btn btn_primary">
                        Zobacz materiał
                    </a>
                </div>

            </div>

        <?php endwhile; ?>
    <?php endif; ?>

</section>