<section class="about-us">

	<div class="container">

		<div class="section_heading">
			<h2><?php the_field('about-us_heading'); ?></h2>
			<p><?php the_field('about-us_paragraph'); ?></p>
		</div>

	</div>


	<div class="categories">

        <?php if ( have_rows('aboutus_elements', 'options') ) : ?>
            <?php while( have_rows('aboutus_elements', 'options') ) : the_row(); ?>

				<div class="categories_item" style="background-image: url('<?php the_sub_field('background', 'options') ?>')">

					<a href="<?php the_sub_field('link', 'options'); ?>">
						<h2><?php the_sub_field('heading', 'options'); ?></h2>
						<p><?php the_sub_field('paragraph', 'options'); ?></p>
					</a>

				</div>

            <?php endwhile; ?>
        <?php endif; ?>

	</div>


</section>