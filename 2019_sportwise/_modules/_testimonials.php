<section class="testimonials">

	<div class="container">

		<div class="section_heading">
			<h2><?php the_field('testimonials_heading', 'options'); ?></h2>
			<p><?php the_field('testimonials_paragraph', 'options'); ?></p>
		</div>


		<div class="testimonials-wrapper">

            <?php if ( have_rows('testimonials_slider', 'options') ) : ?>
            	<?php while( have_rows('testimonials_slider', 'options') ) : the_row(); ?>

					<div class="item">

						<div class="author">

							<?php if (get_sub_field('image', 'options')) { ?>
								<img src=" <?php the_sub_field('image', 'options') ?>" alt="author-img">
                            <?php } ?>

							<div class="info">
								<h5><?php the_sub_field('name', 'options') ?></h5>
								<h6><?php the_sub_field('role', 'options') ?></h6>
							</div>

						</div>

                        <?php the_sub_field('content', 'options') ?>

					</div>

				<?php endwhile; ?>
            <?php endif; ?>

		</div>


	</div>

</section>