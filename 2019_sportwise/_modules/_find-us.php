<section class="find-us">

	<div class="container">

		<div class="box">

			<h6><?php the_field('findus_heading', 'options'); ?></h6>

			<?php the_field('findus_address', 'options'); ?>

			<div class="buttons_wrapper">

				<a id="js-fuPopup" class="btn btn_tertiary">Sprawdź grafik zajęć</a>

			</div>

		</div>

	</div>

	<div class="section_image"></div>

</section>