<section class="news">

	<div class="container">


		<div class="section_heading">
			<h2><?php the_field('mainpage_heading'); ?></h2>
			<p><?php the_field('mainpage_paragraph'); ?></p>
		</div>

		<div id="js-articles">
			<div class="news_wrapper">
				<article v-for="(article, index) in posts.slice(0, 2)" class="news_listing">
					<figure class="news_listing_image" :style="'background-image: url(' + getThumbnail(article) + ');'">
						<a :href="article.link"></a>
					</figure>
					<div class="news_listing_info">
						<a :href="article.link">
							<h5> {{ article.title.rendered }}</h5>

							<p v-html="article.excerpt.rendered"></p>
						</a>
					</div>
				</article>
			</div>
		</div>

		<div class="button_wrapper">
			<a href="<?php the_field('mainpage_url'); ?>" class="btn btn_secondary">Wszystkie aktualności</a>
		</div>

	</div>

</section>