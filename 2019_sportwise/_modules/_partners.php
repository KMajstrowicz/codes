<section class="partners">

    <?php if ( have_rows('partners', 'options') ) : ?>
        <?php while( have_rows('partners', 'options') ) : the_row(); ?>

            <div class="partners_item">

                <div class="container">

					<img src="<?php the_sub_field('image', 'options') ?>" alt="Partner image">
					
                </div>

            </div>

        <?php endwhile; ?>
    <?php endif; ?>

</section>