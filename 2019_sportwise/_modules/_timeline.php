<section class="timeline">

	<div class="container">

		<div class="section_heading">
			<h2><?php the_field('timeline_heading'); ?></h2>
		</div>

		<div class="timeline_boxes">

            <?php if ( have_rows('timeline') ) :
            $number = 1 ?>
            <?php while( have_rows('timeline') ) : the_row(); ?>

				<div class="item">

					<span class="number"><?php echo $number; ?></span>
					<img src="<?php the_sub_field('icon') ?>" alt="icon">
					<p><?php the_sub_field('text') ?></p>

				</div>

            <?php $number++; endwhile; endif; ?>

		</div>

	</div>

</section>