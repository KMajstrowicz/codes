<section class="ski">

    <?php if(get_sub_field('ski_heading')) : ?>
		<div class="container">
			<div class="section_heading">
				<h2><?php the_field('ski_heading'); ?></h2>
			</div>
		</div>
	<?php endif; ?>

	<div class="container">

        <?php if ( have_rows('ski') ) : while( have_rows('ski') ) : the_row();?>

				<article class="ski_item">
					<a href="<?php the_sub_field('url') ?>">

						<figure class="thumbnail" style="background-image: url(<?php the_sub_field('img') ?>)"></figure>

						<?php if(get_sub_field('place')) : ?>
							<h3><?php the_sub_field('place') ?></h3>
						<?php endif; ?>

                        <?php if(get_sub_field('date')) : ?>
							<p><span>Data:</span> <?php the_sub_field('date') ?></p>
                        <?php endif; ?>

                        <?php if(get_sub_field('level')) : ?>
							<p><span>Poziom:</span> <?php the_sub_field('level') ?></p>
                        <?php endif; ?>

						<?php if(get_sub_field('age')) : ?>
							<p><span>Wiek:</span> <?php the_sub_field('age') ?></p>
                        <?php endif; ?>

                        <?php if(get_sub_field('price')) : ?>
							<p class="price"><span>Cena:</span> <?php the_sub_field('price') ?></p>
						<?php endif; ?>

						<p>Więcej informacji</p>

					</a>
				</article>

		<?php endwhile; endif; ?>


	</div>

</section>