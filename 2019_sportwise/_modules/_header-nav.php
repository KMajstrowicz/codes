<nav class="nav">
	<div class="container">

		<div class="nav-wrapper">

			<div class="nav_logo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img src="<?php echo THEME_URL; ?>/assets/images/brand-logo.png" alt="Sportwise logo">
				</a>
			</div>

			<div id="js-menu-toggler" class="nav_burger">
				<span></span>
				<span></span>
				<span></span>
			</div>

			<div class="nav_menu">

                <?php
                wp_nav_menu( array(
                    'theme_location' => 'primary',
                    'menu_class' => 'list',
                    'container'=> 'ul'
                ));
                ?>

			</div>

		</div>

	</div>
</nav>