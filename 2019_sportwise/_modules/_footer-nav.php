<section class="footer-nav">

	<div class="container">

		<div class="col">

            <?php
            	wp_nav_menu( array(
                	'theme_location' => 'secondary',
                	'menu_class' => 'list',
                	'container'=> 'ul'
            	));
            ?>

		</div>

		<div class="col">

            <?php
				wp_nav_menu( array(
					'theme_location' => 'tertiary',
					'menu_class' => 'list',
					'container'=> 'ul'
				));
            ?>

		</div>

		<div class="col">

            <?php
				wp_nav_menu( array(
					'theme_location' => 'quaternary',
					'menu_class' => 'list',
					'container'=> 'ul'
				));
            ?>

		</div>

		<div class="col">

			<h6>Social media</h6>

			<ul class="list">

                <?php if( get_field('socials_fb', 'options') ): ?>

				<li class="list_item">
					<a href="<?php the_field('socials_fb', 'options'); ?>" target="_blank">
						<figure>
							<img src="<?php echo THEME_URL; ?>/assets/images/social-fb.png" alt="icon">
						</figure>
						Facebook
					</a>
				</li>

				<?php endif; ?>

                <?php if( get_field('socials_tw', 'options') ): ?>

					<li class="list_item">
						<a href="<?php the_field('socials_tw', 'options'); ?>" target="_blank">
							<figure>
								<img src="<?php echo THEME_URL; ?>/assets/images/social-twitter.png" alt="icon">
							</figure>
							Twitter
						</a>
					</li>

                <?php endif; ?>

                <?php if( get_field('socials_ig', 'options') ): ?>

				<li class="list_item">
					<a href="<?php the_field('socials_ig', 'options'); ?>" target="_blank">
						<figure>
							<img src="<?php echo THEME_URL; ?>/assets/images/social-insta.png" alt="icon">
						</figure>
						Instagram
					</a>
				</li>

				<?php endif; ?>

			</ul>

		</div>

	</div>

</section>