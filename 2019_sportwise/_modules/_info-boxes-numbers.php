<section class="info-boxes-numbers">

	<div class="container">

		<div class="section_heading">
			<h2><?php the_field('info_boxes-heading'); ?></h2>
		</div>

		<div class="info-boxes-numbers_boxes">

            <?php if ( have_rows('info_boxes') ) : while( have_rows('info_boxes') ) : the_row(); ?>

				<div class="item">

					<p class="primary"><?php the_sub_field('primary') ?></p>
					<p class="secondary"><?php the_sub_field('secondary') ?></p>
					<p class="tertiary"><?php the_sub_field('tertiary') ?></p>

				</div>

            <?php endwhile; endif; ?>

		</div>

	</div>

</section>