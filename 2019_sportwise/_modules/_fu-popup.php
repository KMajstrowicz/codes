<div id="js-harmonograms" class="fu-popup">
    <div class="wrapper">
        <h2>Wybierz swój lokal</h2>
        <div class="loader-container" v-if="loading">
            <div class="loader-image">
                <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/loader.svg' ?>" alt="ładuję...">
            </div>
        </div>
        <div id="js-fuPopupClose" class="close">
            <span class="bar"></span>
            <span class="bar"></span>
        </div>
        <div class="select-wrapper">
            <div class="item" v-if="places.length">
                <select @change="getHarmonograms">
                    <option disabled selected>Wybierz swój lokal</option>
                    <option v-for="place in places" :value="place.id">{{ place.name }}</option>
                </select>
            </div>
            <div class="item">
                <div v-for="harmonogram in harmonograms">
                    <template v-for="group in harmonogram.acf.groupLevel">
                        <h5 class="item-name">{{ harmonogram.title.rendered }}<span>({{ group.groupName }})</span></h5>
                        <ul class="list">
                            <li v-for="day in group.group" class="list_item">
                                <div>
                                    <p>{{ day.weekDay }}</p>
                                    <p>{{ day.hours }}</p>
                                </div>
                            </li>
                            <a href="#" class="btn btn_primary">Zapisz się na zajęcia</a>
                        </ul>
                    </template>
                </div>
            </div>
        </div>
    </div>
</div>
