<section class="groups">

	<div class="container">

		<div class="groups_boxes">

            <?php if ( have_rows('groups') ) : while( have_rows('groups') ) : the_row(); ?>

				<div class="item">

					<a href="<?php the_sub_field('url') ?>">
						<h6><?php the_sub_field('text') ?></h6>
					</a>

				</div>

            <?php endwhile; endif; ?>

		</div>

	</div>

</section>