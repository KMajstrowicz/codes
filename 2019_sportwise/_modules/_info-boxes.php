<section class="info-boxes">

	<div class="container">

		<div class="section_heading">
			<h2><?php the_field('info_boxes-heading'); ?></h2>
		</div>

		<div class="info-boxes_boxes">

            <?php if ( have_rows('info_boxes') ) : while( have_rows('info_boxes') ) : the_row(); ?>

				<div class="item">

					<img src="<?php the_sub_field('icon') ?>" alt="icon">
					<div class="text"><?php the_sub_field('text') ?></div>

				</div>

            <?php endwhile; endif; ?>

		</div>

	</div>

</section>